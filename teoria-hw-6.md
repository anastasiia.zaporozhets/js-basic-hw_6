1. Опишіть своїми словами, що таке метод об'єкту

Методи - це дії які можна виконувати над об'єктами в js.Методи зберігаються у властивостях як визначення функцій. Тобто
метод об'єкту - це функція, яка є властивістю об'єкта і може викликатися через цей об'єкт. Вони дозволяють об'єктам
виконувати дії або виконувати операції на своїх властивостях або інших об'єктах.

2. Який тип даних може мати значення властивості об'єкта?

Значення властивості об'єкта може бути будь-якого типу даних, що підтримує JavaScript, включаючи інші об'єкти, масиви та
функції. Тобто немає обмежень щодо типів даних які можуть бути властивостями об'єкта.

3. Об'єкт це посилальний тип даних. Що означає це поняття?

Об'єкт це посилальний тип даних- це означає, що коли ми присвоююємо об'єкт одній змінній і потім присвоюємо цю змінну
іншій, обидві змінні посилаються на один і той самий об'єкт у пам'яті.Тобто зміни в одному об'єкті також впливають на
всі інші змінні, які посилаються на цей об'єкт.
Це відрізнається від примітивних типів даних, таких як числа, рядки та булеві значення, які копіюються при присвоєнні
іншій змінній.
