"use strict"

//завдання перше
let product = {
    name: 'Laptop',
    price: 15000,
    discount: 2000,

    getDiscountPrice: function () {
        return this.price - this.discount;
    }
}

let discountPrice = product.getDiscountPrice();

console.log(`Ціна зі знижкою ${discountPrice} грн`);


// завдання друге
function getUserNameAndAge(person) {
    let name = person.name;
    let age = person.age;
    // return `Привіт ${name} тобі ${age} років!`;
    if (isNaN(age)){
        return `Привіт ${name} тобі ${age} років!`;
    } else {
        return `Привіт ${name} тобі ${age} років!`;
    }
}

let person = {
    name: prompt("Напишіть Ваше імʼя"),
    age: prompt("Напишіть Ваш вік")
};

alert(getUserNameAndAge(person));
console.log(getUserNameAndAge(person));


//завдання три

function cloneObject(obj) {

    if (typeof obj !== 'object' || obj === null) {
        return obj;
    }

    let clone;

    if (Array.isArray(obj)) {
        clone = [];
        for (let i = 0; i < obj.length; i++) {
            clone[i] = cloneObject(obj[i]);
        }
    } else {
        clone = {};

        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                clone[key] = cloneObject(obj[key]);
            }
        }
    }

    return clone;
}

//приклад клонування із перевіркою в консолі масиву та обʼєкта

let car = {
    carBrand: "BMW",
    model: "X5",
    year: 2016,
    color: "Black"
}

console.log(car);

let cloneCarObj = cloneObject(car);
cloneCarObj.engine = "Diesel";

console.log(cloneCarObj);

let arr = [1, 2, 3, 4, 5];
console.log(arr);

let cloneArr = cloneObject(arr);
cloneArr.push(4);
console.log(cloneArr);





